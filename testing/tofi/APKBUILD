# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=tofi
# https://github.com/philj56/tofi/issues/2
pkgver=0_git20220627
_gitrev=3a2178ab907d05dd10c482d7b0c8ce5ca6f3bb7a
pkgrel=0
pkgdesc="Tiny dynamic menu for Wayland"
url="https://github.com/philj56/tofi"
arch="all"
license="MIT"
makedepends="
	cairo-dev
	freetype-dev
	harfbuzz-dev
	libxkbcommon-dev
	meson
	pango-dev
	scdoc
	wayland-dev
	wayland-protocols
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	"
source="https://github.com/philj56/tofi/archive/$_gitrev/$pkgname-$_gitrev.tar.gz"
builddir="$srcdir/$pkgname-$_gitrev"
options="!check"  # no tests provided

build() {
	abuild-meson . output
	meson compile -j ${JOBS:-0} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
d9acd7ae52bb764d8c96bd4ffd7d9de3e3a1415d1a6d1be3baecd8d175e630a71f72ced41e039ec9963da54496c89ebb380490be040f8f173ec07f4721f757c7  tofi-3a2178ab907d05dd10c482d7b0c8ce5ca6f3bb7a.tar.gz
"
